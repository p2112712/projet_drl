import gym
from gym.wrappers.monitoring.video_recorder import VideoRecorder
import matplotlib.pyplot as plt


# to run : pipenv run python filename




'''
class Step3():

    env = gym.make('CartPole-v1')
    for i_episode in range(10):
        state = env.reset()
        inter_num = 0
        rewards = 0
        while True: 
            env.render()
            #print(state)
            action = env.action_space.sample()
            state, reward, done, info = env.step(action)
            inter_num = inter_num + 1 
            rewards = rewards + reward
            if done:
                break
        print("Episode : " + str(i_episode) + " ; nb interactions : " + str(inter_num) + " ; recompenses : " +str(rewards))

    env.close()
'''

class Step4():

    # évaluation graph
    def plotLearning(x, scores,filename, lines=None):
        fig = plt.figure()
        ax=fig.add_subplot(111, label="1")
        ax.plot(x, scores, color="C0")
        ax.set_xlabel("Game", color="C0")
        ax.set_ylabel("Rewards Avrg", color="C0")
        ax.tick_params(axis='x', colors="C0")
        ax.tick_params(axis='y', colors="C0")
        if lines is not None:
            for line in lines:
                plt.axvline(x=line)

        plt.savefig(filename)


    #enviren initialisation
    env = gym.make('CartPole-v1')
    env.seed(0)

    # video recording
    video_recorder = None
    video_recorder = VideoRecorder(env,'./video_CartPolev1_Random-Agent.mp4', enabled=True)
    
    etapes = []
    all_rewards = []

    # starting episodes 
    for i_episode in range(50):
        state = env.reset()
        inter_num = 0
        rewards = 0
        while True: 
            env.render()
            video_recorder.capture_frame()
            #print(state)
            action = env.action_space.sample()
            state, reward, done, info = env.step(action)
            inter_num = inter_num + 1 
            rewards = rewards + reward
            if done:
                break
        print("Episode : " + str(i_episode) + " ; nb interactions : " + str(inter_num) + " ; recompenses : " +str(rewards))

        all_rewards.append(rewards)
        etapes.append(i_episode)


    print("Saved video.")
    video_recorder.close()
    video_recorder.enabled = False
    filename = 'CartPolev1_Random-Agent.png'
    #plotLearning(all_rewards,etapes, filename)
    plotLearning(etapes,all_rewards, filename)
    env.close()


 
   



def main():
    #step3 = Step3()
    step4 = Step4()
    
if __name__ == '__main__':
    main()





