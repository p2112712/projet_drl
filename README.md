# Projet_DRL

# TP_bio_inpired
Ce TP a été réalisé par CHBAB Khalid ( p2112712) et BOUBKER Ayoub (p2113144)

Vous trouverez notre rapport "Rapport_Boubker_Chbab.pdf" concernant le TP dans les fichiers.


Notre projet est divisé par 2 parties :
    1- le fichier "one.py" qui représente notre agent aléatoire.
    2- le fichier "Q_Network" représente la partie 2 contenant l'implémentation du Deep Q_Network
        Les paramètres actuels sont les suivants :
            - BATCH_SIZE = 32
            - BUFFER_SIZE = 100000
            - MIN_REPLAY_SIZE = 1000
            - EPSILON_START = 1.0
            - EPSILON_END  = 0.02
            - EPSILON_DECAY = 10000
            - TARGET_UPDATE_FREQ = 1000
            - EPISODES = 50000


### Changement d'environnement
Nous n'avons malheureusement pas pu installer l'environnement "MineRL".
